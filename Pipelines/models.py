from django.db import models
from Jobs.models import Fusilli_Jobs
from django.utils import timezone


# Create your models here.
class Fusilli_Jobs_Configuration(models.Model):
    job = models.OneToOneField(Fusilli_Jobs, on_delete=models.PROTECT)
    replicas = models.IntegerField(null=False)
    replicas_stopped = models.IntegerField(null=False, default=0)
    created_on = models.DateTimeField(default=timezone.now, null=False)

    def __str__(self):
        return 'Fusilli_Jobs_Configuration'


class Fusilli_Pipeline(models.Model):
    instance = models.IntegerField(null=False)
    pipeline = models.IntegerField(null=False)
    configuration_json = models.TextField(null=True)
    job_configuration = models.ManyToManyField(Fusilli_Jobs_Configuration, default=None, related_name='pipeline_job_configuration')
    created_on = models.DateTimeField(default=timezone.now, null=False)

    def __str__(self):
        return 'Fusilli_Pipeline'
