from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from Kubernetes.services.test_kubernetes import start_test, delete_deployment
from Pipelines.services.pipeline_services import __check_pipeline_configuration_json, __start_pipeline, __stop_pipeline
import logging
# Get an instance of a logger
logger = logging.getLogger(__name__)


@api_view(['GET'])
def start_test_kubernetes(request):
    http_status = status.HTTP_200_OK
    data = start_test()
    return Response(data, http_status)


@api_view(['GET'])
def end_test_kubernetes(request):
    http_status = status.HTTP_200_OK
    data = delete_deployment()
    return Response(data, http_status)


# start single pipeline
@api_view(['POST'])
def start_pipeline(request):
    pipeline_configuration = request.data

    if pipeline_configuration:
        data, http_status = __check_pipeline_configuration_json(pipeline_configuration)

        if http_status == status.HTTP_200_OK:
            __start_pipeline(pipeline_configuration)

    else:
        message = 'Cannot deploy Pipeline on Kubernetes, configuration missing'
        logger.error(message)
        http_status = status.HTTP_500_INTERNAL_SERVER_ERROR
        data = {
            'status': http_status,
            'message': message
        }
    return Response(data, http_status)


# start single pipeline
@api_view(['POST'])
def stop_pipeline(request):
    pipeline_configuration = request.data

    if pipeline_configuration:

        data, http_status = __stop_pipeline(pipeline_configuration)

    else:
        message = 'Cannot deploy Pipeline on Kubernetes, configuration missing'
        logger.error(message)
        http_status = status.HTTP_500_INTERNAL_SERVER_ERROR
        data = {
            'status': http_status,
            'message': message
        }
    return Response(data, http_status)
