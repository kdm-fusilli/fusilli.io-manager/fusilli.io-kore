from .view import pipeline_view
from django.conf.urls import url


urlpatterns = [
    # insert new Pipeline
    url('start/', pipeline_view.start_pipeline, name='start_pipeline'),
    url('stop/', pipeline_view.stop_pipeline, name='stop_pipeline'),
    url('test/', pipeline_view.start_test_kubernetes, name='start_test_kubernetes'),
    url('end/', pipeline_view.end_test_kubernetes, name='end_test_kubernetes'),
]