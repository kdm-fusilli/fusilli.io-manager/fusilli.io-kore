from rest_framework import status
from Kubernetes.services.kubernetes_services import __create_deployment_object, __deploy_on_kubernetes, \
    __undeploy_on_kubernetes, get_kafka_configuration
from Jobs.models import Fusilli_Jobs
from Logger.services.logger_services import __logger_pipeline_internal_status
from fusilliiokore.settings import STATUS_LEVEL_SETUP, MESSAGE_DEPLOYING_JOB, FUSILLI_KORE_VERSION, FUSILLI_LOGGER_TOPIC
import json
import time
import copy
from kubernetes import client
import logging

# Get an instance of a logger
OUTPUT_FIELD = 'output'
JOB_FIELD = 'job'
INPUT_FIELD = 'input'
logger = logging.getLogger(__name__)


def __get_generic_env_var(single_config, job_configuration_id, instance_id, pipeline_id, policies_id, policies,
                          kafka_configuration):
    var_name = ''
    input_edges_json = {'input_edges': []}
    output_edges_json = {'output_edges': []}
    fusilli_logger_json = copy.deepcopy(kafka_configuration)
    fusilli_logger_json['topic'] = FUSILLI_LOGGER_TOPIC

    if single_config.get('purpose') == JOB_FIELD:
        var_name = 'job_config'
        single_config_env = json.dumps(single_config.get('job_configs'))
        input_edges_value = single_config['input_edges'][0]
        output_edges_value = single_config['output_edges'][0]
        topic_input = 'fusilli_queue_' + str(input_edges_value)
        topic_output = 'fusilli_queue_' + str(output_edges_value)

        # input_edges
        current_kafka_configuration_input = copy.deepcopy(kafka_configuration)
        current_kafka_configuration_input['topic'] = topic_input
        input_edges_json['input_edges'].append(current_kafka_configuration_input)

        # output_edges
        current_kafka_configuration_output = copy.deepcopy(kafka_configuration)
        current_kafka_configuration_output['topic'] = topic_output
        output_edges_json['output_edges'].append(current_kafka_configuration_output)

    elif single_config.get('purpose') == INPUT_FIELD:
        var_name = 'connector_config'
        single_config_env = json.dumps(single_config)
        connection_edge = single_config['entities'][0]['connection_edge']
        topic = 'fusilli_queue_' + str(connection_edge)
        # output_edges
        current_kafka_configuration_output = copy.deepcopy(kafka_configuration)
        current_kafka_configuration_output['topic'] = topic
        output_edges_json['output_edges'].append(current_kafka_configuration_output)

    elif single_config.get('purpose') == OUTPUT_FIELD:
        var_name = 'connector_config'
        single_config_env = json.dumps(single_config)
        connection_edge = single_config['entities'][0]['connection_edge']
        topic = 'fusilli_queue_' + str(connection_edge)

        # input_edges
        current_kafka_configuration_input = copy.deepcopy(kafka_configuration)
        current_kafka_configuration_input['topic'] = topic
        input_edges_json['input_edges'].append(current_kafka_configuration_input)

    list_envs = [
        client.V1EnvVar(var_name, single_config_env),
        client.V1EnvVar('job_configuration_id', str(job_configuration_id)),
        client.V1EnvVar('instance_id', str(instance_id)),
        client.V1EnvVar('pipeline_id', str(pipeline_id)),
        client.V1EnvVar('policies_id', str(policies_id)),
        client.V1EnvVar('version', str(FUSILLI_KORE_VERSION)),
        client.V1EnvVar('fusilli_logger', json.dumps(fusilli_logger_json)),
        client.V1EnvVar('input_edges', json.dumps(input_edges_json)),
        client.V1EnvVar('output_edges', json.dumps(output_edges_json))
    ]

    if int(job_configuration_id) < 0:
        list_envs.append(client.V1EnvVar('policies', json.dumps(policies)))

    logger.debug('Environment job configuration will be: ' + str(list_envs))
    return list_envs


def __get_generic_deployment_json(single_job, generic_deployment_name, prefix):
    generic_job_deployment_json = {
        'container_image': '',
        'container_name': prefix + '-' + generic_deployment_name,
        'deployment_name': prefix + '-' + generic_deployment_name,
        'image_pull_policy': single_job.get('image_pull_policy', 'Always'),
        'image_restart_policy': single_job.get('image_restart_policy', 'Always')
        # 'image_pull_policy': single_job.get('image_pull_policy', 'IfNotPresent')
    }
    logger.debug('generic_job_deployment_json: ' + json.dumps(generic_job_deployment_json))
    return generic_job_deployment_json


def __get_generic_deployment_name(instance_id, pipeline_id, job_configuration_id):
    generic_deployment_name = [str(job_configuration_id), str(pipeline_id), str(instance_id)]
    return '-'.join(generic_deployment_name)


def __deploy_inputs(logger_api, instance_id, pipeline_id, policies_id, inputs, kafka_configuration, deploy=False):
    logger.info('Start to deploy inputs on Kubernetes')
    for single_input in inputs:
        job_configuration_id = single_input.get('job_configuration_id', 0)
        generic_deployment_name = __get_generic_deployment_name(instance_id, pipeline_id, job_configuration_id)
        prefix = ['fusilli', INPUT_FIELD, single_input.get('repo_type')]
        deployment_json = __get_generic_deployment_json(single_input, generic_deployment_name, '-'.join(prefix))
        fusilli_job_model = Fusilli_Jobs.objects.get(name=single_input.get('repo_type'))
        deployment_json['container_image'] = fusilli_job_model.image
        single_input['purpose'] = INPUT_FIELD
        logger.debug('generic_job_deployment_json: ' + json.dumps(deployment_json))
        env = __get_generic_env_var(single_input, job_configuration_id, instance_id, pipeline_id, policies_id, None,
                                    kafka_configuration)
        logger.debug('entities: ' + str(single_input.get('entities', '')))
        env.append(client.V1EnvVar(name='entities', value=str(single_input.get('entities', ''))))
        input_deployment = __create_deployment_object(deployment_json, env)

        if deploy:
            __deploy_on_kubernetes(input_deployment)
        else:
            __undeploy_on_kubernetes(input_deployment)

        __logger_pipeline_internal_status(logger_api, instance_id, pipeline_id, policies_id, job_configuration_id,
                                          STATUS_LEVEL_SETUP, MESSAGE_DEPLOYING_JOB)
    logger.info('Inputs deployed on Kubernetes')
    pass


def __deploy_jobs(logger_api, instance_id, pipeline_id, policies_id, policies, jobs, kafka_configuration, deploy=False):
    logger.info('Start to deploy jobs on Kubernetes')
    job_list = []
    for single_job in jobs:
        job_configuration_id = single_job.get('job_configuration_id', 0)
        generic_deployment_name = __get_generic_deployment_name(instance_id, pipeline_id, job_configuration_id)
        prefix = ['fusilli', JOB_FIELD, single_job.get('job_name')]
        deployment_json = __get_generic_deployment_json(single_job, generic_deployment_name, '-'.join(prefix))
        fusilli_job_model = Fusilli_Jobs.objects.get(name=single_job.get('job_name'))
        deployment_json['container_image'] = fusilli_job_model.image
        single_job['purpose'] = JOB_FIELD
        env = __get_generic_env_var(single_job, job_configuration_id, instance_id, pipeline_id, policies_id, policies,
                                    kafka_configuration)
        replicas = single_job.get('job_instances', 1)
        job_deployment = __create_deployment_object(deployment_json, env, replicas)

        if deploy:
            __deploy_on_kubernetes(job_deployment)
        else:
            __undeploy_on_kubernetes(job_deployment)

        __logger_pipeline_internal_status(logger_api, instance_id, pipeline_id, policies_id, job_configuration_id,
                                          STATUS_LEVEL_SETUP, MESSAGE_DEPLOYING_JOB)
    logger.info('Jobs deployed on Kubernetes')
    return job_list


def __deploy_outputs(logger_api, instance_id, pipeline_id, policies_id, outputs, kafka_configuration, deploy=False):
    logger.info('Start to deploy outputs on Kubernetes')
    for single_output in outputs:
        logger.debug('Single output to deploy: ' + json.dumps(single_output))
        job_configuration_id = single_output.get('job_configuration_id', 0)
        generic_deployment_name = __get_generic_deployment_name(instance_id, pipeline_id, job_configuration_id)
        prefix = ['fusilli', OUTPUT_FIELD, single_output.get('repo_type')]
        deployment_json = __get_generic_deployment_json(single_output, generic_deployment_name, '-'.join(prefix))
        fusilli_job_model = Fusilli_Jobs.objects.get(name=single_output.get('repo_type'))
        deployment_json['container_image'] = fusilli_job_model.image
        single_output['purpose'] = OUTPUT_FIELD
        env = __get_generic_env_var(single_output, job_configuration_id, instance_id, pipeline_id, policies_id, None,
                                    kafka_configuration)
        try:
            entity_instances = single_output.get('entities')[0]
            replicas = entity_instances.get('entity_instances', 1)
        except:
            replicas = single_output.get('entity_instances', 1)

        output_deployment = __create_deployment_object(deployment_json, env, replicas)

        if deploy:
            __deploy_on_kubernetes(output_deployment)
        else:
            __undeploy_on_kubernetes(output_deployment)

        __logger_pipeline_internal_status(logger_api, instance_id, pipeline_id, policies_id, job_configuration_id,
                                          STATUS_LEVEL_SETUP, MESSAGE_DEPLOYING_JOB)

    logger.info('Outputs deployed on Kubernetes')
    pass


def __check_pipeline_configuration_json(pipeline_configuration):
    http_status = status.HTTP_200_OK
    message = 'The Pipeline will be deployed on Kubernetes'
    data = {
        'status': http_status,
        'message': message
    }
    return data, http_status


def __start_pipeline(json_configuration):
    logger.info('Start read pipeline configuration')

    try:
        instance_id, kafka_configuration, logger_api, pipeline_id, pipeline_json_configration, policies_id, policies = __get_pipeline_configuration(json_configuration)

        logger.info('deploying inputs')
        inputs = pipeline_json_configration.get('inputs')
        if inputs is not None:
            __deploy_inputs(logger_api, instance_id, pipeline_id, policies_id, inputs, kafka_configuration, True)
            logger.info('input deployed')
        ## TODO: create single input models from job_list and update piepeline_model

        jobs = pipeline_json_configration.get('jobs')
        if jobs is not None:
            __deploy_jobs(logger_api, instance_id, pipeline_id, policies_id, policies, jobs, kafka_configuration, True)
            logger.info('jobs deployed')
        ## TODO: create single job models from job_list and update piepeline_model

        # policies = pipeline_json_configration.get('policies', None)
        outputs = pipeline_json_configration.get('outputs')
        if outputs is not None:
            __deploy_outputs(logger_api, instance_id, pipeline_id, policies_id, outputs, kafka_configuration, True)
            logger.info('output deployed')
        ## TODO: create single output models from job_list and update piepeline_model

    except:
        message = 'Cannot deploy pipeline on Kubernetes'
        logger.error(message)
    pass


def __stop_pipeline(json_configuration):
    logger.info('Start read pipeline configuration')

    try:
        instance_id, kafka_configuration, logger_api, pipeline_id, pipeline_json_configration, policies_id, policies = __get_pipeline_configuration(json_configuration)

        inputs = pipeline_json_configration.get('inputs')
        if inputs is not None:
            __deploy_inputs(logger_api, instance_id, pipeline_id, policies_id, inputs, kafka_configuration, False)
            logger.info('input deployed')

        jobs = pipeline_json_configration.get('jobs')
        if jobs is not None:
            __deploy_jobs(logger_api, instance_id, pipeline_id, policies_id, jobs, kafka_configuration, False)
            logger.info('jobs deployed')

        # policies = pipeline_json_configration.get('policies', None)
        outputs = pipeline_json_configration.get('outputs')
        if outputs is not None:
            __deploy_outputs(logger_api, instance_id, pipeline_id, policies_id, None, outputs, kafka_configuration, False)
            logger.info('output deployed')

    except:
        message = 'Cannot stop pipeline on Kubernetes'
        logger.error(message)
        http_status = status.HTTP_500_INTERNAL_SERVER_ERROR
        data = {
            'status': http_status,
            'message': message
        }
        return data, http_status

    message = 'Pipeline deleted from Kubernetes'
    logger.info(message)

    http_status = status.HTTP_200_OK
    data = {
        'status': http_status,
        'message': message
    }
    return data, http_status


def __get_pipeline_configuration(json_configuration):
    pipeline_json_configration = dict(json_configuration)
    logger.info('retrieving configuration')
    logger_api = pipeline_json_configration.get('logger', None)
    instance_id = pipeline_json_configration.get('instance_id', 0)
    pipeline_id = pipeline_json_configration.get('pipeline_id', 0)
    policies_id = pipeline_json_configration.get('policies_id', 'none')
    policies = pipeline_json_configration.get('policies', None)
    kafka_configuration = get_kafka_configuration()
    return instance_id, kafka_configuration, logger_api, pipeline_id, pipeline_json_configration, policies_id, policies
