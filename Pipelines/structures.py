from Pipelines.models import Fusilli_Pipeline
from django.utils import timezone
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)


def __get_pipeline_model(instance_id, pipeline_id, configuration_json):
    pipeline = Fusilli_Pipeline(instance=instance_id,
                                pipeline=pipeline_id,
                                configuration_json=configuration_json,
                                created_on=timezone.now())

    return pipeline
