from django.shortcuts import render
from django.db import connection
from django.shortcuts import redirect
import re
from Kubernetes.services.kubernetes_services import get_kafka_configuration, __get_services_list, __get_pods_list
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)


# Create your views here.
def index(request):
    # kafka_configuration = {
    #     'host': 'localhost',
    #     'port': 9092,
    #     'group_id': 'group-id',
    #     'topic': '',
    #     'user': 'username',
    #     'password': 'password',
    #     'kafka_namespace': 'default',
    #     'kafka_name': 'kafka-bootstrap'
    # }
    kafka_configuration = get_kafka_configuration()
    results = {
        'fusilli_k8s_data_back_plane': {},
        'fusilli_k8s_services': [],
        'fusilli_k8s_pods': []
    }
    try:
        fusilli_k8s_data_back_plane = get_kafka_configuration()
        fusilli_k8s_data_back_plane['kafka_namespace'] = fusilli_k8s_data_back_plane.get('kafka_namespace').replace('my-kafka-project', 'fusilli-kafka-backplane')
        fusilli_k8s_data_back_plane['kafka_name'] = fusilli_k8s_data_back_plane.get('kafka_name').replace('my-cluster-kafka-bootstrap', 'fusilli-cluster-kafka-bootstrap')
        results['fusilli_k8s_data_back_plane'] = fusilli_k8s_data_back_plane
    except:
        logger.error('Cannot retrieve fusilli_k8s_data_back_plane')

    try:
        fusilli_k8s_services = __get_services_list()
        results['fusilli_k8s_services'] = fusilli_k8s_services
    except:
        logger.error('Cannot retrieve fusilli_k8s_services')

    try:
        fusilli_k8s_pods = __get_pods_list()
        results['fusilli_k8s_pods'] = fusilli_k8s_pods
    except:
        logger.error('Cannot retrieve fusilli_k8s_pods')

    return render(request, 'index.html', {'results': results})


def __get_line_data():
    data = []
    labels = []
    return data, labels


def __get_chart_data():
    labels = []
    data = []
    return data, labels

def __get_table_data():
    colum_name_array = []
    tables = []
    return tables, colum_name_array
