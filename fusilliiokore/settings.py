"""
Django settings for fusilliiokore project.

Generated by 'django-admin startproject' using Django 3.1.7.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.1/ref/settings/
"""

from pathlib import Path
import os
from configparser import RawConfigParser

config = RawConfigParser()
config.read('fusilli_kore_configuration.ini')

# FUSILLI
try:
    FUSILLI_KORE_VERSION = os.environ['FUSILLI_KORE_VERSION']
except:
    FUSILLI_KORE_VERSION = config.get('fusilli', 'FUSILLI_KORE_VERSION')
try:
    FUSILLI_LOGGER_TOPIC = os.environ['FUSILLI_LOGGER_TOPIC']
except:
    FUSILLI_LOGGER_TOPIC = config.get('fusilli_pipelines_logger', 'FUSILLI_LOGGER_TOPIC')
try:
    FUSILLI_LOGGER_URLS = os.environ['FUSILLI_LOGGER_URLS']
except:
    FUSILLI_LOGGER_URLS = config.get('fusilli_pipelines_logger', 'FUSILLI_LOGGER_URLS')
try:
    STATUS_LEVEL_SETUP = os.environ['STATUS_LEVEL_SETUP']
except:
    STATUS_LEVEL_SETUP = config.get('fusilli_pipelines_logger', 'STATUS_LEVEL_SETUP')
try:
    MESSAGE_DEPLOYING_JOB = os.environ['MESSAGE_DEPLOYING_JOB']
except:
    MESSAGE_DEPLOYING_JOB = config.get('fusilli_pipelines_logger', 'MESSAGE_DEPLOYING_JOB')
try:
    DEPLOY_STATUS = os.environ['DEPLOY_STATUS']
except:
    DEPLOY_STATUS = config.get('kubernetes', 'DEPLOY_STATUS')
# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'twxw3h4&io=1j$wn9u+xc1fb#9yv7#-535ie$w_&^o48kh--h@'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

try:
    LOGGING_LEVEL = os.environ['LOGGING_LEVEL']
except:
    LOGGING_LEVEL = config.get('logging', 'LOGGING_LEVEL')

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',

            'level': LOGGING_LEVEL,
            'formatter': 'simpleRe',
        },
    },
    'root': {
        'handlers': ['console'],
        'level': LOGGING_LEVEL,
    },
    'formatters': {
        'simpleRe': {
            'format': '{levelname} {asctime} [{module} - line: {lineno}] : {message}',
            'style': '{',
        }

    }
}

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = [
    'rest_framework',
    'corsheaders',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'Dashboard.apps.DashboardConfig',
    'Kubernetes.apps.KubernetesConfig',
    'Pipelines.apps.PipelinesConfig',
    'Jobs.apps.JobsConfig',
    'Logger.apps.LoggerConfig'
]

MIDDLEWARE = [
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'fusilliiokore.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates']
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'fusilliiokore.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        # 'NAME': ':memory:',
        'NAME': str(BASE_DIR / 'db.sqlite3'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.1/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'collected_static')

MEDIA_URL = '/mediafiles/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'static/mediafiles')

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]

TEMPLATE_DIRS = (
    os.path.join(BASE_DIR, 'templates'),
)

