# fusilli-kore

WARNING: This allows any user with read access to secrets or the ability to create a pod to access super-user credentials.

```bash
kubectl create clusterrolebinding serviceaccounts-cluster-admin --clusterrole=cluster-admin --group=system:serviceaccounts
```

https://kubernetes.io/docs/admin/authorization/rbac/

