from django.apps import AppConfig
import json


class JobsConfig(AppConfig):
    name = 'Jobs'

    def ready(self):
        from Jobs.models import Fusilli_Jobs
        Fusilli_Jobs.objects.all().delete()
        with open('templates/jobs.json') as f:
            data = json.load(f)
            jobs = data.get('jobs')
            for job in jobs:
                job_model = Fusilli_Jobs(name=job.get('name'), image=job.get('image'))
                job_model.save()

        # job_1 = Fusilli_Jobs(
        #     name='sftp',
        #     image='registry.gitlab.com/kdm-fusilli/fusilli.io-manager/fusilli.io-connectors/fusilli.io-sftp:latest'
        # )
        # job_1.save()
        #
        # job_2 = Fusilli_Jobs(
        #     name='postgresql',
        #     image='registry.gitlab.com/kdm-fusilli/fusilli.io-manager/fusilli.io-connectors/fusilli.io-postgresql:latest'
        # )
        # job_2.save()
