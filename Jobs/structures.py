from Jobs.models import Fusilli_Jobs
from django.utils import timezone
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)


def __get_job_model(job_json):
    job_id = job_json.get('id', 0)
    if job_id == 0:
        job = Fusilli_Jobs(name=job_json['name'],
                           image=job_json['image'],
                           created_on=timezone.now())
    else:
        job = Fusilli_Jobs(id=job_id, name=job_json['name'],
                           image=job_json['image'],
                           created_on=job_json['created_on'])
    return job
