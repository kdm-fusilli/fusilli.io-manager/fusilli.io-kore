from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
import logging
from Jobs.models import Fusilli_Jobs

# Get an instance of a logger
logger = logging.getLogger(__name__)


@api_view(['POST'])
def save_new_job(request):
    message = 'Cannot deploy Pipeline on Kubernetes, configuration missing'
    logger.error(message)
    http_status = status.HTTP_500_INTERNAL_SERVER_ERROR
    data = {
        'status': http_status,
        'message': message
    }

    return Response(data, http_status)


@api_view(['POST'])
def delete_job(request):
    message = 'Cannot deploy Pipeline on Kubernetes, configuration missing'
    logger.error(message)
    http_status = status.HTTP_500_INTERNAL_SERVER_ERROR
    data = {
        'status': http_status,
        'message': message
    }

    return Response(data, http_status)



@api_view(['GET'])
def get_all_jobs(request):

    http_status = status.HTTP_200_OK
    jobs = []
    jobs_model = Fusilli_Jobs.objects.all()
    for job_model in jobs_model:
        job = {
            'name': job_model.name,
            'image': job_model.image
        }
        jobs.append(job)
    data = {
        'status': http_status,
        'results': jobs
    }

    return Response(data, http_status)
