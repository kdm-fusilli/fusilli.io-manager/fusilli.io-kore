from django.db import models
from django.utils import timezone


# Create your models here.
class Fusilli_Jobs(models.Model):
    name = models.CharField(max_length=50, null=False)
    image = models.TextField(null=False)
    created_on = models.DateTimeField(default=timezone.now, null=False)

    def __str__(self):
        return 'Fusilli_Jobs'
