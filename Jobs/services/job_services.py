from rest_framework import status

from Jobs.models import Fusilli_Jobs
from Jobs.structures import __get_job_model


def __save_new_job(job_data):
    job_obj = Fusilli_Jobs.objects.filter(name=job_data.get('name')).values()
    if job_obj:
        data = {
            "result": 2,
            "message": "Insert job failed. Job name already exists"
        }
        http_status = status.HTTP_409_CONFLICT

    else:
        new_job = __get_job_model(job_data)
        new_job.save()
        job_configuration_data = job_data.get('job_configuration_data')
        if job_configuration_data:
            __save_new_job_configuration_template(job_configuration_data, new_job.id)
        data = {
            'result': 0,
            'message': 'Insert Job Success'
        }
        http_status = status.HTTP_200_OK
    return data, http_status
