from .view import job_view
from django.conf.urls import url


urlpatterns = [
    # insert new Pipeline
    url('save/', job_view.save_new_job, name='save_new_job'),
    url('delete/', job_view.delete_job, name='delete_job'),
    url('all/', job_view.get_all_jobs, name='get_all_jobs'),
]