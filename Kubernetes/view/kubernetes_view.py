import logging

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from Kubernetes.services.kubernetes_services import __get_pods_list, __get_services_list, get_kafka_configuration

# Get an instance of a logger
logger = logging.getLogger(__name__)


@api_view(['GET'])
def get_pod_list(request):
    http_status = status.HTTP_200_OK

    data = __get_pods_list()
    return Response(data, http_status)


@api_view(['GET'])
def get_service_list(request):
    http_status = status.HTTP_200_OK

    data = __get_services_list()
    return Response(data, http_status)


@api_view(['GET'])
def get_kafka_configuration_in_fusilli(request):
    http_status = status.HTTP_200_OK

    data = get_kafka_configuration()
    return Response(data, http_status)
