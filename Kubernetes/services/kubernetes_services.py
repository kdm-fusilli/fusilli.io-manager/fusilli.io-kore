from kubernetes import client, config
import json
import logging
from fusilliiokore.settings import DEPLOY_STATUS

# Get an instance of a logger
logger = logging.getLogger(__name__)


def __get_pods_list():
    # Configs can be set in Configuration class directly or using helper utility
    logger.info("Start kube configuration")
    config.load_incluster_config()
    logger.info("kube configuration completed")
    v1 = client.CoreV1Api()
    ret = v1.list_pod_for_all_namespaces(watch=False)
    array = []
    for i in ret.items:
        single_element = {
            'pod_id': i.status.pod_ip,
            'pod_namespace': i.metadata.namespace.replace('my-kafka-project', 'fusilli-kafka-backplane'),
            'pod_name': i.metadata.name.replace('my-cluster', 'fusilli-cluster'),
        }
        array.append(single_element)
    return array


def __get_services_list():
    # Configs can be set in Configuration class directly or using helper utility
    logger.info("Start kube configuration")
    config.load_incluster_config()
    logger.info("kube configuration completed")
    v1 = client.CoreV1Api()
    ret = v1.list_service_for_all_namespaces(watch=False)
    array = []
    for i in ret.items:
        single_element = {
            'self_link': i.metadata.self_link,
            'cluster_name': i.metadata.name.replace('my-cluster', 'fusilli-cluster'),
            'cluster_namespace': i.metadata.namespace.replace('my-kafka-project', 'fusilli-kafka-backplane'),
            'cluster_ip': i.spec.cluster_ip
        }
        array.append(single_element)
    return array


def get_kafka_configuration():
    kafka_configuration = {
        'host': 'localhost',
        'port': 9092,
        'group_id': 'group-id',
        'topic': '',
        'user': 'username',
        'password': 'password',
        'kafka_namespace': 'default',
        'kafka_name': 'kafka-bootstrap'
    }
    try:
        logger.info("Load in cluster Kubernetes configuration to retrieve Kafka configs")
        config.load_incluster_config()
        logger.info("Load in cluster Kubernetes configuration completed")
        v1 = client.CoreV1Api()
        ret = v1.list_service_for_all_namespaces(watch=False)
        for i in ret.items:
            if 'kafka-bootstrap' in i.metadata.name:
                kafka_configuration['host'] = i.spec.cluster_ip
                kafka_configuration['kafka_namespace'] = i.metadata.namespace
                kafka_configuration['kafka_name'] = i.metadata.name
                logger.debug('Kafka configurations: ' + json.dumps(kafka_configuration))

        logger.info("Kafka configuration retrieved")
        return kafka_configuration
    except:
        logger.error("Load in cluster Kubernetes configuration error")
        logger.warning("It will be used default kafka configuration")
        return kafka_configuration


def __create_deployment_object(deployment_json, env=None, replicas=1):
    # Configure Pod template container
    logger.debug('Creating container with following configurations')
    container = client.V1Container(
        name=deployment_json.get('deployment_name'),
        image=deployment_json.get('container_image'),
        env=env,
        image_pull_policy=deployment_json.get('image_pull_policy'),
        ports=[client.V1ContainerPort(container_port=80)]
    )

    # Create and configurate a spec section
    template = client.V1PodTemplateSpec(
        metadata=client.V1ObjectMeta(labels={'app': deployment_json.get('deployment_name')}),
        spec=client.V1PodSpec(containers=[container], restart_policy=deployment_json.get('image_restart_policy')))

    # Create the specification of deployment
    spec = client.V1DeploymentSpec(
        replicas=replicas,
        template=template,
        selector={'matchLabels': {'app': deployment_json.get('deployment_name')}})

    # Instantiate the deployment object
    deployment = client.V1Deployment(
        api_version="apps/v1",
        kind="Deployment",
        metadata=client.V1ObjectMeta(name=deployment_json.get('deployment_name'),
                                     labels={'app': deployment_json.get('deployment_name')}),
        spec=spec)

    return deployment


def __deploy_on_kubernetes(deployment, namespace='default'):
    if DEPLOY_STATUS == 'True':
        try:
            config.load_incluster_config()
            apps_v1 = client.AppsV1Api()
            # k8s_api_obj = client.CoreV1Api()
            # apps_v1 = None
            logger.debug('Creating deployment ....')
            # Create deployement
            api_response = apps_v1.create_namespaced_deployment(
                body=deployment,
                namespace=namespace)
            logger.debug("Deployment created. status='%s'" % str(api_response.status))
        except:
            message = 'Cannot load Kubernetes cluster config'
            logger.error(message)
    else:
        logger.debug('You are in debug mode. Your Pipeline will not deploy on Kubernetes')


def __undeploy_on_kubernetes(deployment, namespace='default'):
    if DEPLOY_STATUS == 'True':
        try:

            config.load_incluster_config()
            api_instance = client.AppsV1Api()
            # Delete deployment
            api_response = api_instance.delete_namespaced_deployment(
                name=deployment.get('deployment_name'),
                namespace=namespace,
                body=client.V1DeleteOptions(
                    propagation_policy='Foreground',
                    grace_period_seconds=5))
            logger.debug("Deployment deleted. status='%s'" % str(api_response.status))
        except:
            message = 'Cannot load Kubernetes cluster config'
            logger.error(message)
    else:
        logger.debug('You are in debug mode. Your Pipeline will not deploy on Kubernetes')
    pass


def __get_generic_namespace_name(namespace):
    generic_namespace_name = [str("fusilli-pipeline"), str(namespace)]
    return '-'.join(generic_namespace_name)
