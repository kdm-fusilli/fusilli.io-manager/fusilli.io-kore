from Kubernetes.services.kubernetes_services import get_kafka_configuration
from Jobs.models import Fusilli_Jobs
from fusilliiokore.settings import DEPLOY_STATUS, FUSILLI_LOGGER_TOPIC, FUSILLI_LOGGER_URLS
from kubernetes import client, config
import traceback
import logging
import json

# Get an instance of a logger
logger = logging.getLogger(__name__)

DEPLOYMENT_NAME = 'fusilli-io-logger'


def create_deployment_object(env=None, fusilli_logger_image=None):
    logger.debug('Start Create deploy ' + DEPLOYMENT_NAME)
    logger.debug('Environment job configuration will be: ' + str(env))

    try:
        # Configureate Pod template container
        container = client.V1Container(
            name=DEPLOYMENT_NAME,
            image=fusilli_logger_image,
            env=env,
            ports=[client.V1ContainerPort(container_port=80)]
        )
        # Create and configurate a spec section
        template = client.V1PodTemplateSpec(
            metadata=client.V1ObjectMeta(labels={'app': DEPLOYMENT_NAME}),
            spec=client.V1PodSpec(containers=[container]))
        # Create the specification of deployment
        spec = client.V1DeploymentSpec(
            replicas=1,
            template=template,
            selector={'matchLabels': {'app': DEPLOYMENT_NAME}})
        # Instantiate the deployment object
        deployment = client.V1Deployment(
            api_version='apps/v1',
            kind='Deployment',
            metadata=client.V1ObjectMeta(name=DEPLOYMENT_NAME),
            spec=spec)
    except:
        traceback.print_exc()

    return deployment


def create_deployment(deployment):
    config.load_incluster_config()
    api_instance = client.AppsV1Api()

    # Create deployement
    logger.debug('Start Create Deployment ' + DEPLOYMENT_NAME)
    try:
        api_response = api_instance.create_namespaced_deployment(
            body=deployment,
            namespace='default')
        logger.debug('Deployment ' + DEPLOYMENT_NAME + ' created')
    except:
        traceback.print_exc()


def deploy_logger():
    # Configs can be set in Configuration class directly or using helper
    # utility. If no argument provided, the config will be loaded from
    # default location.
    kafka_configuration = get_kafka_configuration()
    kafka_configuration['topic'] = FUSILLI_LOGGER_TOPIC
    list_envs = [
        client.V1EnvVar('FUSILLI_KAFKA_CONFIGURATION', json.dumps(kafka_configuration)),
        client.V1EnvVar('FUSILLI_LOGGER_URLS', FUSILLI_LOGGER_URLS)
    ]
    fusilli_logger_model = Fusilli_Jobs.objects.get(name='fusilli-logger')
    deployment = create_deployment_object(list_envs, fusilli_logger_model.image)
    if DEPLOY_STATUS == 'True':
        create_deployment(deployment)
