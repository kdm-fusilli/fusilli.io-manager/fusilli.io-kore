from kubernetes import client, config
import traceback
import logging
# Get an instance of a logger
logger = logging.getLogger(__name__)

DEPLOYMENT_NAME = "nginx-deployment"


def create_deployment_object():
    logger.debug('Start Create deploy ngnix')
    try:
        # Configureate Pod template container
        container = client.V1Container(
            name="nginx",
            image="nginx:1.15.4",
            ports=[client.V1ContainerPort(container_port=80)],
            resources=client.V1ResourceRequirements(
                requests={"cpu": "100m", "memory": "200Mi"},
                limits={"cpu": "500m", "memory": "500Mi"}
            )
        )
        # Create and configurate a spec section
        template = client.V1PodTemplateSpec(
            metadata=client.V1ObjectMeta(labels={"app": "nginx"}),
            spec=client.V1PodSpec(containers=[container]))
        # Create the specification of deployment
        spec = client.V1DeploymentSpec(
            replicas=3,
            template=template,
            selector={'matchLabels': {'app': 'nginx'}})
        # Instantiate the deployment object
        deployment = client.V1Deployment(
            api_version="apps/v1",
            kind="Deployment",
            metadata=client.V1ObjectMeta(name=DEPLOYMENT_NAME),
            spec=spec)
    except:
        traceback.print_exc()

    return deployment


def create_deployment(api_instance, deployment):
    # Create deployement
    logger.debug('Start Create Deployment ngnix')
    try:
        api_response = api_instance.create_namespaced_deployment(
            body=deployment,
            namespace="default")
        logger.debug("Deployment created. status='%s'" % str(api_response.status))
    except:
        traceback.print_exc()

def delete_deployment():
    config.load_incluster_config()
    api_instance = client.AppsV1Api()
    # Delete deployment
    try:
        logger.debug('Delete ngnix Deployment')
        api_response = api_instance.delete_namespaced_deployment(
            name=DEPLOYMENT_NAME,
            namespace="default",
            body=client.V1DeleteOptions(
                propagation_policy='Foreground',
                grace_period_seconds=5))
        print("Deployment deleted. status='%s'" % str(api_response.status))
    except:
        traceback.print_exc()

def start_test():
    # Configs can be set in Configuration class directly or using helper
    # utility. If no argument provided, the config will be loaded from
    # default location.
    config.load_incluster_config()
    apps_v1 = client.AppsV1Api()

    # Uncomment the following lines to enable debug logging
    # c = client.Configuration()
    # c.debug = True
    # apps_v1 = client.AppsV1Api(api_client=client.ApiClient(configuration=c))

    # Create a deployment object with client-python API. The deployment we
    # created is same as the `nginx-deployment.yaml` in the /examples folder.
    deployment = create_deployment_object()

    create_deployment(apps_v1, deployment)


