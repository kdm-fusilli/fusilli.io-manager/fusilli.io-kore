from .view import kubernetes_view
from django.conf.urls import url


urlpatterns = [
    # insert new Pipeline
    url('pods-list/', kubernetes_view.get_pod_list, name='get_pod_list'),
    url('services-list/', kubernetes_view.get_service_list, name='get_service_list'),
    url('kafka-configuration/', kubernetes_view.get_kafka_configuration_in_fusilli, name='get_kafka_configuration_in_fusilli'),
]