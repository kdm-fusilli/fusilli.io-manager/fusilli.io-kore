from django.apps import AppConfig


class LoggerConfig(AppConfig):
    name = 'Logger'

    def ready(self):
        from Kubernetes.services.logger_kubernetes import deploy_logger
        deploy_logger()
    #     from Logger.services.logger_services import send_log_to_api_no_url
    #     send_log_to_api_no_url()

        # for message in consumer:
        #     logger_message = json.loads(message.value)
        # consumer.close()
        #     pipeline_id = logger_message.get('pipeline_id', 0)
        #     instance_id = logger_message.get('instance_id', 0)
        #     if int(pipeline_id) > 0 and int(instance_id) > 0:
        #         send_log_to_api_no_url(logger_message)
        #     #     pipeline = Fusilli_Pipeline.objects.get(pipeline=int(pipeline_id), instance=int(instance_id))
        #     #     configuration_json = json.loads(pipeline.configuration_json)
        #     #     send_log_to_api(configuration_json.get('logger'), logger_message)

