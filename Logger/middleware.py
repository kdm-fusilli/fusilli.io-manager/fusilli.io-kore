from kafka import KafkaConsumer
from fusilliiokore.settings import FUSILLI_LOGGER_TOPIC
from Pipelines.models import Fusilli_Pipeline
from Logger.services.logger_services import send_log_to_api
from Kubernetes.services.kubernetes_services import get_kafka_configuration
import json
import logging

# Get an instance of a logger
logger = logging.getLogger(__name__)


# 'log_data': {
#   "instance_id": "unique id",
#   "pipeline_id": "pipeline_id",
#   "policies_id": "policies_id",
#   "version": "versione di Fusilli in uso",
#   "job_configuration_id": "unique_job_id",
#   "status": "string",
#   "message": "string",
#   "timestamp": "number"
#   "percentage": "number"
# }
class RequestLoggerMiddleware:
    def __init__(self, get_response):
        logging.info('Init Logger Middleware')
        logger.info('Consumer Logger Starting ...')
        kafka_configuration = get_kafka_configuration()
        kafka_ip = kafka_configuration.get('host', 'localhost')
        kafka_port = kafka_configuration.get('port', '9092')
        kafka_server = kafka_ip + ':' + str(kafka_port)
        logger.debug('Consumer Logger will start with this configuration: ' + str(kafka_server))
        consumer = KafkaConsumer(FUSILLI_LOGGER_TOPIC, group_id='group-id', bootstrap_servers=[kafka_server])
        for message in consumer:
            logger_message = json.loads(message.value)
            pipeline_id = logger_message.get('pipeline_id', 0)
            instance_id = logger_message.get('instance_id', 0)
            # if int(pipeline_id) > 0 and int(instance_id) > 0:
            #     pipeline = Fusilli_Pipeline.objects.get(pipeline=int(pipeline_id), instance=int(instance_id))
            #     configuration_json = json.loads(pipeline.configuration_json)
            #     send_log_to_api(configuration_json.get('logger'), logger_message)
