from kafka import KafkaConsumer
from fusilliiokore.settings import FUSILLI_LOGGER_TOPIC, FUSILLI_KORE_VERSION
from Pipelines.models import Fusilli_Pipeline
from Kubernetes.services.kubernetes_services import get_kafka_configuration
import requests
import json
import time
import logging
# Get an instance of a logger
logger = logging.getLogger(__name__)


# 'log_data': {
#   "instance_id": "unique id",
#   "pipeline_id": "pipeline_id",
#   "policies_id": "policies_id",
#   "version": "versione di Fusilli in uso",
#   "job_configuration_id": "unique_job_id",
#   "status": "string",
#   "message": "string",
#   "timestamp": "number"
#   "percentage": "number"
# }
def __logger_pipeline_internal_status(logger_url=None, instance_id=0, pipeline_id=0, policies_id=0,
                                      job_configuration_id=0, status_level='', message='', percentage=0):
    logger_message = {
        'instance_id': instance_id,
        'pipeline_id': pipeline_id,
        'policies_id': policies_id,
        'version': FUSILLI_KORE_VERSION,
        'job_configuration_id': job_configuration_id,
        'status': status_level,
        'message': message,
        'timestamp': round(time.time() * 1000),
        'percentage': percentage
    }
    logger.info('Logger API will be: ' + str(logger_url))
    logger.info('This message will be send to logger API: ' + json.dumps(logger_message))
    if logger_url is not None:
        send_log_to_api(logger_url, logger_message)


def send_log_to_api(logger_url, logger_message):
    data = {"key": "s@g*kjbc^0@07w48)bvvssor*b70s%2bm+b%c6d^dxfj&8)f_*", "log_data": logger_message}
    headers = {'Content-type': 'application/json'}
    requests.post(url=logger_url, data=json.dumps(data), headers=headers)

# def send_log_to_api_no_url():
#     logger.info('Consumer Logger Starting ...')
#     kafka_configuration = get_kafka_configuration()
#     kafka_ip = kafka_configuration.get('host', 'localhost')
#     kafka_port = kafka_configuration.get('port', '9092')
#     kafka_server = kafka_ip + ':' + str(kafka_port)
#     logger.debug('Consumer Logger will start with this configuration: ' + str(kafka_server))
#     consumer = KafkaConsumer(FUSILLI_LOGGER_TOPIC, group_id='group-id', bootstrap_servers=[kafka_server], max_poll_interval_ms=5000)
#     # asyncio.run(get_message(consumer))
#     # requests.post(url=logger_url, data=logger_message)

# async def get_message(consumer):
#     for message in consumer:
#         logger_message = json.loads(message.value)
#         logger.info('This message will be send to logger API: ' + json.dumps(logger_message))